export const environment = {
  production: true
};

export const API_URL = "https://fakerestapi.azurewebsites.net/";

export const API_ENDPOINTS = {
  BOOKS: "api/Books",
  AUTHORS: "api/Authors",
  AUTHORS_BOOKS: "authors/books"
};
