// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const API_URL = "https://fakerestapi.azurewebsites.net/";

export const API_ENDPOINTS = {
  BOOKS: "api/Books",
  AUTHORS: "api/Authors",
  AUTHORS_BOOKS: "authors/books"
};
