import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {BookListComponent} from "./book-list/book-list.component";
import {NotFoundComponent} from "./not-found/not-found.component";
import {BookDetailComponent} from "./book-detail/book-detail.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: "/home",
    pathMatch: "full"
  },
  {
    path: "home",
    component: BookListComponent
  },
  {
    path: "home/:id",
    component: BookDetailComponent
  },
  {
    path: "**",
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContentRoutingModule {
}
