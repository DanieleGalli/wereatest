import {Component, OnInit, ViewChild} from "@angular/core";
import {ApiService} from "../services/api.service";
import {API_ENDPOINTS} from "../../../environments/environment";
import {Book} from "../models/book.model";
import {MatPaginator, MatSort, MatTableDataSource, PageEvent} from "@angular/material";
import {forkJoin, Observable} from "rxjs";
import {BookListItem} from "../models/book-list.model";
import {Author} from "../models/author.model";
import {ShortAuthor} from "../models/short-author.model";


@Component({
  selector: "app-book-list",
  templateUrl: "./book-list.component.html",
  styleUrls: ["./book-list.component.scss"]
})
export class BookListComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns: string[] = ["BookId", "Title", "Authors", "Action"];
  dataSource: MatTableDataSource<BookListItem>;
  bookList: BookListItem[] = new Array();

  length = 200;
  pageSize = 20;
  pageSizeOptions: number[] = [5, 10, 25, 50, 100, 200];

  pageEvent: PageEvent;

  constructor(private api: ApiService) {
  }

  ngOnInit() {
    this.getBookDetails().subscribe(response => {
      const resp1: Book[] = response[0];
      const resp2: Author[] = response[1];

      resp1.forEach(book => {
        const bookListItem: BookListItem = new BookListItem();
        bookListItem.BookId = book.ID;
        bookListItem.Title.Book = book.Title;
        bookListItem.Title.PageCount = book.PageCount;
        bookListItem.Title.Published = new Date(book.PublishDate); //book.PublishDate;
        bookListItem.Description = book.Description;
        bookListItem.Excerpt = book.Excerpt;
        const authorBook = resp2.filter(author => author.IDBook === book.ID);
        if (authorBook && authorBook != null && authorBook.length > 0) {
          authorBook.forEach(auth => {
            const shortAuth = new ShortAuthor();
            shortAuth.name = auth.FirstName;
            shortAuth.surname = auth.LastName;
            bookListItem.Authors.push(shortAuth);
          });
        }
        this.bookList.push(bookListItem);
      });
      this.dataSource = new MatTableDataSource(this.bookList);
      this.dataSource.paginator = this.paginator;
    });
  }

  private getBookDetails(): Observable<any> {
    return forkJoin(this.api.get(API_ENDPOINTS.BOOKS), this.api.get(API_ENDPOINTS.AUTHORS));
  }
}
