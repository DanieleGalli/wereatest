import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {API_URL} from "../../../environments/environment";
import {Observable} from "rxjs";


@Injectable({
  providedIn: "root"
})
export class ApiService {

  constructor(private http: HttpClient) {
  }

  public get(url): Observable<any> {
    return this.http.get(API_URL + url);
  }

  public post(url): Observable<any> {
    const options = {
      headers: new HttpHeaders({ "Content-Type": "application/json" })
    };

    return this.http.post(API_URL + url, options);
  }
}
