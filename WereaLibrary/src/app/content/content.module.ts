import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";

import {ContentRoutingModule} from "./content-routing.module";
import {BookListComponent} from "./book-list/book-list.component";
import {NotFoundComponent} from "./not-found/not-found.component";
import {BookDetailComponent} from "./book-detail/book-detail.component";
import {ApiService} from "./services/api.service";
import {HttpClientModule} from "@angular/common/http";
import {MatTableModule} from "@angular/material/table";
import {MatButtonModule, MatCardModule, MatPaginatorModule} from "@angular/material";

@NgModule({
  declarations: [BookListComponent, NotFoundComponent, BookDetailComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    MatCardModule,
    MatTableModule,
    MatButtonModule,
    MatPaginatorModule,
    ContentRoutingModule
  ],
  exports: [BookListComponent],
  providers: [ApiService]
})
export class ContentModule {
}
