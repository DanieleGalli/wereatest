export class BookListTitle {
  Book: string;
  Published: Date;
  PageCount: number;

  constructor() {
    this.Book = "";
    this.Published = new Date();
    this.PageCount = 0;
  }
}
