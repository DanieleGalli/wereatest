import {BookListTitle} from "./book-list-title.model";
import {ShortAuthor} from "./short-author.model";

export class BookListItem {
  BookId: number;
  Title: BookListTitle;
  Authors: ShortAuthor[];
  Description: string;
  Excerpt: string;

  constructor() {
    this.BookId = 0;
    this.Title = new BookListTitle();
    this.Authors = new Array();
    this.Description = "";
    this.Excerpt = "";
  }
}
