export class Book {
  ID: number;
  Title: string;
  Description: string;
  PageCount: 0;
  Excerpt: string;
  PublishDate: string;//Date//"2018-12-18T18:04:31.115Z"

  constructor() {
    this.ID = 0;
    this.Title = "";
    this.Description = "";
    this.PageCount = 0;
    this.Excerpt = "";
    this.PublishDate = "";
  }
}
