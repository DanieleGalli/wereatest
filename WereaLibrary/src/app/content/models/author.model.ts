export class Author {
  ID: number;
  IDBook: number;
  FirstName: string;
  LastName: string;

  constructor() {
    this.ID = 0;
    this.IDBook = 0;
    this.FirstName = "";
    this.LastName = "";
  }
}
