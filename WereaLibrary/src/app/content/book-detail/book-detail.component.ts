import {Component, OnInit} from "@angular/core";
import {ApiService} from "../services/api.service";
import {forkJoin, Observable} from "rxjs";
import {API_ENDPOINTS} from "../../../environments/environment";
import {ActivatedRoute, Router} from "@angular/router";
import {Book} from "../models/book.model";
import {Author} from "../models/author.model";

@Component({
  selector: "app-book-detail",
  templateUrl: "./book-detail.component.html",
  styleUrls: ["./book-detail.component.scss"]
})
export class BookDetailComponent implements OnInit {
  private bookId;
  title;
  authors = "";
  pageCount;
  description;
  excerpt;
  published;

  constructor(private api: ApiService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.bookId = this.route.snapshot.paramMap.get("id");
    this.getBookDetail().subscribe(response => {
      const book: Book = response[0];
      const authors: Author[] = response[1];
      this.title = book.Title;
      this.pageCount = book.PageCount;
      this.description = book.Description;
      this.excerpt = book.Excerpt;
      this.published = new Date(book.PublishDate);
      // this.authors = authors;
      if (authors && authors.length > 0) {
        for (let index = 0; index < authors.length; index++) {
          this.authors += "Author " + (index + 1) + " " + authors[index].FirstName + " " + authors[index].LastName;
          if (authors.length > 1 && (index + 1 < authors.length)) {
            this.authors += ", ";
          }
        }
      }
    });
  }

  private getBookDetail(): Observable<any> {
    return forkJoin(this.api.get(API_ENDPOINTS.BOOKS + "/" + this.bookId), this.api.get(API_ENDPOINTS.AUTHORS_BOOKS + "/" + this.bookId));
  }

  public back() {
    this.router.navigate(["home"]);
  }

}
